using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;

namespace Stack
{
    public class Stack
    {
       public List<object> List = new List<object>();
        public void Push(object obj)
        {
            if (obj == null)
                throw new Exception("null");
            List.Add(obj);
           
        }

        public object Pop()
        {
            if (List == null)
            {
                throw new Exception("list doesnt contain anything");
           }

            var lastIndex = List[List.Count-1];
            List.Remove(List[List.Count - 1]);
            return lastIndex;
        }

        private void Clear()
        {
            Console.Clear();

        }
    }
}