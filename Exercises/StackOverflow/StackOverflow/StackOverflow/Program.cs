﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackOverflow
{
    class Program
    {
        static void Main(string[] args)
        {
            var post = new Post();
            
            post.Title = "title";
            post.Description = "description";
            post.UpVote();
            post.UpVote();
            post.DownVote();
            post.Display();
        }
    }
}
