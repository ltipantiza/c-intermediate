﻿using System;

namespace StackOverflow
{
    public class Post
    {
        private readonly DateTime _created = DateTime.Now;
        private int _vote = 0;
        
        public string Title { get; set; }
        public string Description { get; set; }

        
        public int UpVote()
        {
           return _vote++; 
        }
        
        public int DownVote()
        {
                return _vote--;
        }

        public void Display()
        {
            Console.WriteLine(Title);
            Console.WriteLine(Description);
            Console.WriteLine(_created);
            Console.WriteLine(_vote);
        }

    }
}