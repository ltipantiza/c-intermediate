﻿using System;

namespace Stop_Watch
{
	public class Stopwatch
	{
		private DateTime _startWatch;
		private DateTime _stopWatch;
		private TimeSpan _duration;
		private bool _watchOn;

		public void Start()
		{
				if (_watchOn)
					throw new InvalidOperationException("watch already started");
				
					_startWatch = DateTime.Now;
					_watchOn = true;
				
		}

		public void Stop()
		{
			if (!_watchOn)
				throw new Exception("turn on the watch first");

			_stopWatch = DateTime.Now;
			_watchOn = false;
		}

		public void Duration()
		{
			if (_watchOn)
			{
				throw new Exception("cannot get duration without stopping the watch");
			}
			else
			{
				_duration = _stopWatch - _startWatch;
				Console.WriteLine("the duration is {0}", _duration);
			}
		}
		
	}
}