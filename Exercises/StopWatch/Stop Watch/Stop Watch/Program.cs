﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stop_Watch
{
    class Program
    {
        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            while (true)
            {
                var input = Console.ReadLine();
                if (input.ToLower() == "start")
                {
                    stopwatch.Start();
                }
                else if (input.ToLower() == "stop")
                {
                    stopwatch.Stop();
                    stopwatch.Duration();
                }
            }
            
        }
    }
}
