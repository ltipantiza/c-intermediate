﻿using System;

namespace stop_watch_2nd_try
{
    class Stopwatch
    {
        private DateTime _startTime;
        private DateTime _stopTime;
        private bool _isOn;
        private TimeSpan _duration;

        private TimeSpan Duration
        {
            get
            {
               return _duration = _stopTime - _startTime;
            }
        }
        
        public void Start()
        {
            if (!_isOn)
            {
                _startTime = DateTime.Now;
                _isOn = true;
            }
            else
            {
                throw new Exception("Stopwatch is already on");
            }
        }

        public void Stop()
        {
            if (_isOn)
            {
                _stopTime = DateTime.Now;
                Console.WriteLine(Duration);
                _isOn = false;
            }
            else
            {
                    throw new Exception("Stopwatch isn't turned on yet");
            }
        }
    }
}