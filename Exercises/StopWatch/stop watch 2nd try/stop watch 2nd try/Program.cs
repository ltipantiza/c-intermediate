﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stop_watch_2nd_try
{
    class Program
    {
        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();

            while (true)
            {
                Console.WriteLine("Type in start to start the Stopwatch. Then Type stop to stop your stopwatch. ");
                var input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "start":
                        stopwatch.Start();
                        break;
                    case "stop":stopwatch.Stop();
                        break;
                    default:
                        throw new Exception("Must type valid command");

                }
            }
        }
    }
}
