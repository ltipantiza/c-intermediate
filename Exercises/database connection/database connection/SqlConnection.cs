using System;

namespace database_connection
{
    class SqlConnection : DbConnection
    {
        public SqlConnection(string connectingString) 
            : base(connectingString)
        {
            Console.WriteLine("Sql data accessed");
        }

        public override void Open()
        {
            Console.WriteLine("opening sql");
        }
        

        public override void Close()
        {
            Console.WriteLine("closing sql");
        }
    }
}