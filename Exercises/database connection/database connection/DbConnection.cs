using System;

namespace database_connection
{
    abstract class DbConnection
    {
        protected TimeSpan TimeOut { get; set; }
        protected string ConnectingString { get; set; }

        protected DbConnection(string connectingString)
        {
            if (string.IsNullOrWhiteSpace(connectingString))
            {
                throw new Exception("must be a valid string");
            }

            this.ConnectingString = connectingString;
        }

        public abstract void Open();
        public abstract void Close();


    }
}