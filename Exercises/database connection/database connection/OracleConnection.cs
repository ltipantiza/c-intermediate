using System;

namespace database_connection
{
    class OracleConnection : DbConnection
    {
        public OracleConnection(string connectingString) 
            : base(connectingString)
        {
            Console.WriteLine("Oracle data accessed");
        }

        public override void Open()
        {
            Console.WriteLine("Opening Oracle");
            
        }
        public override void Close()
        {
            Console.WriteLine("Closing Oracle");
        }
    }
}