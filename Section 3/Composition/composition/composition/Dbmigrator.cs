﻿namespace composition
{
    public class Dbmigrator
    {
        private readonly Logger _logger;

        public Dbmigrator(Logger logger)
        {
            _logger = logger;
        }

        public void migrate()
        {
            _logger.Log("we are migrating blah blah blah...");
        }
    }
}