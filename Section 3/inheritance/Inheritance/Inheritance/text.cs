﻿using System;

namespace Inheritance
{
    public class text : PresentationObject
    {
        public int TextSize { get; set; }
        public string FontName { get; set; }

        public void AddHyperLink(string url)
        {
            Console.WriteLine("we added a link to "+ url);
        }
    }
}