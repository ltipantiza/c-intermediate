﻿using System;

namespace Inheritance
{
    public class PresentationObject
    {
        public int width { get; set; }
        public int height { get; set; }

        public void Copy()
        {
            Console.WriteLine("object copied to clip board");
        }

        public void Duplicate()
        {
            Console.WriteLine("object was duplicated");
        }
    }
}